"""
Grammar:
S' -> S
S -> NP VP
S -> epsilon
NP -> N S
VP -> V N
N -> n
V -> v

S' -> S -> NP VP -> N S VP -> cats S VP -> cats eps VP -> cats V N -> cats saw N -> cats saw alice
"""
import enum

alphabet = ["alice", "cats", "saw", "grinned"]
nouns = ["alice", "cats"]
verbs = ["saw", "grinned"]

FIRST = {
    "S_prime": nouns,
    "S": nouns,
    "NP": nouns,
    "VP": verbs,
    "N": nouns,
    "V": verbs
}

FOLLOW = {
    "S_prime": ["$"],
    "S": ["saw", "grinned", "$"],
    "NP": verbs,
    "VP": ["saw", "grinned", "$"],
    "N": ["alice", "cats", "saw", "grinned", "$"],
    "V": ["alice", "cats", "$"]
}

NULLABLE = {
    "S_prime": None,
    "S": True,
    "NP": False,
    "VP": False,
    "N": False,
    "V": False
}


class States(enum.Enum):
    S0 = 0
    S1 = 1
    S2 = 2
    S3 = 3
    S4 = 4
    S5 = 5
    S6 = 6
    S7 = 7
    S8 = 8
    S9 = 9
    S10 = 10
    S11 = 11
    S12 = 12
    S13 = 13
    S14 = 14
    S15 = 15
    ACCEPT = 16  # Accept


def action(state, terminal):

    print("Action state: " + str(state) + " terminal: " + str(terminal))

    if state == States.S0:
        if terminal in nouns:
            return "shift", States.S4
        elif terminal == "$":
            return "reduce", 2
    elif state == States.S1:
        if terminal == "$":
            return States.ACCEPT
    elif state == States.S2:
        if terminal in verbs:
            return "shift", States.S7
    elif state == States.S3:
        if terminal in nouns:
            return "shift", States.S4
        elif terminal in verbs:
            return "reduce", 2
    elif state == States.S4:
        if terminal in verbs or terminal in nouns:
            return "reduce", 5
    elif state == States.S5:
        if terminal == "$":
            return "reduce", 1
    elif state == States.S6:
        if terminal in nouns:
            return "shift", States.S11
    elif state == States.S7:
        if terminal in nouns:
            return "reduce", 6
    elif state == States.S8:
        if terminal in verbs:
            return "reduce", 3
    elif state == States.S9:
        if terminal in verbs:
            return "shift", States.S7
    elif state == States.S10:
        if terminal == "$":
            return "reduce", 4
    elif state == States.S11:
        if terminal == "$":
            return "reduce", 5
    elif state == States.S12:
        if terminal in verbs:
            return "reduce", 1
    elif state == States.S13:
        if terminal in nouns:
            return "shift", States.S15
    elif state == States.S14:
        if terminal in verbs:
            return "reduce", 4
    elif state == States.S15:
        if terminal in verbs:
            return "reduce", 5

    return None


def goto(state, nonterminal):

    print("Goto state: " + str(state) + " nonterminal: " + str(nonterminal))

    if state == States.S0:
        if isinstance(nonterminal, S):
            return States.S1
        elif isinstance(nonterminal, NP):
            return States.S2
        elif isinstance(nonterminal, N):
            return States.S3
    elif state == States.S1:
        pass
    elif state == States.S2:
        if isinstance(nonterminal, VP):
            return States.S5
        elif isinstance(nonterminal, V):
            return States.S6
    elif state == States.S3:
        if isinstance(nonterminal, S):
            return States.S8
        elif isinstance(nonterminal, NP):
            return States.S9
        elif isinstance(nonterminal, N):
            return States.S3
    elif state == States.S4:
        pass
    elif state == States.S5:
        pass
    elif state == States.S6:
        if isinstance(nonterminal, N):
            return States.S10
    elif state == States.S7:
        pass
    elif state == States.S8:
        pass
    elif state == States.S9:
        if isinstance(nonterminal, VP):
            return States.S12
        elif isinstance(nonterminal, V):
            return States.S13
    elif state == States.S10:
        pass
    elif state == States.S11:
        pass
    elif state == States.S12:
        pass
    elif state == States.S13:
        if isinstance(nonterminal, N):
            return States.S14
    elif state == States.S14:
        pass
    elif state == States.S15:
        pass
    elif state == States.ACCEPT:
        return States.ACCEPT

    return None


class S_dash:
    def __init__(self, s):
        self.s = s


class S:  # S -> NP VP | S -> epsilon
    def __init__(self, np, vp):
        self.np = np
        self.vp = vp
    


class NP:  # NP -> N S
    def __init__(self, n, s):
        self.n = n
        self.s = s


class VP:  # VP -> V N
    def __init__(self, v, n):
        self.v = v
        self.n = n


class N:
    def __init__(self, n):
        self.n = n


class V:
    def __init__(self, v):
        self.v = v



def parse(sentence):
    tokens = ("^ " + sentence).lower().replace('.', '').split()
    tokens.append("$")

    parse_stack = [States.S0]
    state = States.S0

    for i in range(len(tokens)):
        # Shift to read the next token
        token = tokens[i]
        if token == "$":
            return 0  # Accept

        parse_stack.append(token)
        lookahead = tokens[i+1]

        continue_ = True
        first = True

        while continue_:

            print("__" + str(i) + "__")
            print(parse_stack)
            print(tokens)

            # I think the times when I'm checking action vs goto is wrong too as this
            # checks action too often
            # I'll have a look at this again if I have time, but no guarantees about that!
            
            next_action = action(state, lookahead)
            if next_action == None:
                next_action = goto(state, lookahead)
                
            print("Next action is " + str(next_action))

            if next_action == None:
                raise Exception("Cannot parse token " + token)

            elif next_action == States.ACCEPT:
                print("Done")
                break

            elif next_action[0] == "shift":
                state = next_action[1]
                parse_stack.append(state)

                continue_ = False

            elif next_action[0] == "reduce":
                reduction = next_action[1]

                """
                The structure of parse_stack is
                    [S1 NP VP].
                """

                # S' -> S
                if reduction == 0:
                    o1 = parse_stack.pop()
                    o1 = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, S_dash(o1))
                    parse_stack.append(new_state)
                    parse_stack.append(S_dash(o1))

                    state = new_state

                # S -> NP VP
                elif reduction == 1:
                    vp1 = parse_stack.pop()
                    vp1 = parse_stack.pop()
                    np1 = parse_stack.pop()
                    np1 = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, S(np1, vp1))
                    parse_stack.append(new_state)
                    parse_stack.append(S(np1, vp1))

                    state = new_state

                # S -> ε
                elif reduction == 2:
                
                    state = parse_stack[len(parse_stack)-2]
                    state = goto(state, S(None, None))      # Is this the right state transition?
                                                # I don't think it matters in practice, but
                    parse_stack.append(state)   # it would be nice to have the right state
                    parse_stack.append(S(None, None))

                # NP -> N S
                elif reduction == 3:
                    s1 = parse_stack.pop()
                    s1 = parse_stack.pop()
                    n1 = parse_stack.pop()
                    n1 = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, NP(n1,s1))
                    parse_stack.append(new_state)
                    parse_stack.append(NP(n1, s1))

                    state = new_state

                # VP -> V N
                elif reduction == 4:
                    s1 = parse_stack.pop()
                    s1 = parse_stack.pop()
                    n1 = parse_stack.pop()
                    n1 = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, VP(v1,n1))
                    parse_stack.append(new_state)
                    parse_stack.append(VP(v1, n1))

                    state = new_state

                # N -> n
                elif reduction == 5:
                    n1 = parse_stack.pop()
                    old_state = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, N(n1))
                    parse_stack.append(new_state)
                    parse_stack.append(N(n1))

                    state = new_state

                # V -> v
                elif reduction == 6:
                    v1 = parse_stack.pop()
                    v1 = parse_stack.pop()
                    old_state = parse_stack[len(parse_stack)-2]

                    new_state = goto(old_state, V(v1))
                    parse_stack.append(new_state)
                    parse_stack.append(V(v1))

                    state = new_state


parse("Cats cats saw alice saw alice")
